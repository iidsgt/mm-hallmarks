FROM rocker/verse:4.0.3
RUN apt-get update
# change this to install the packages required
RUN Rscript -e "install.packages(c('stringr','tidyverse','BiocManager'), dependencies=TRUE, repos = 'http://cran.us.r-project.org')"
# add your script to the container
ADD trialcheck.r /bin/
RUN chmod +x /bin/trialcheck.r
CMD ["/bin/bash"]

