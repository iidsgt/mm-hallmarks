# current (yet to add Special case MM-PSN) code
options(stringsAsFactors = FALSE)

# Parameterizing
args <- commandArgs(trailingOnly = TRUE)

prog_mark_table <- args[1]
dnaSampleId <- args[2]
transloc_table <- args[3]
facetncyt_table <- args[4]
scar_score_path <- args[5]
annotated_variants_path <- args[6]
output_path <- args[7]
fusions_table <- args[8]

if (length(args) != 8) {
  stop("Seven arguments must be supplied. Please enter paths to Prognostic Markers Table, sample ID, 
  translocation table, cnv band table, scar score file, annotated variants file, output file path, and fusions table results.")
}

# testing paths
## # comment for final version
## dnaSampleId <- "MM_6080_BM_01"
## prog_mark_table <- "prognosticMarkersTable.tsv"
## transloc_table <- "predicted_translocations.csv"
## facetncyt_table <-  "cnv_per_band.csv"
## scar_score_path <- "scarScore_latest.tsv"
## annotated_variants_path <-  "annotated_variants.consensus.vcf"
## output_path <- '.'
## fusions_table <- "fusions.tsv"

# libraries
library(tidyverse)
library(stringr)

# PROGNOSTIC MARKERS
# prognosticMarkerList is the reference table
prognosticMarkerList <- read.table(file = prog_mark_table, sep = '\t', header = TRUE) 

# TRANSLOCATIONS
translocation <- read.table(transloc_table, sep = ',', header = TRUE, row.names=1)
if (!is.na(translocation) && length(translocation) > 0) {
  transloc2 <- data.frame(t(translocation))
  transloc2$genes <- rownames(transloc2)
  rownames(transloc2) <- NULL
  transloc2$genes <- gsub("_trans", "", transloc2$genes) #removes _trans from gene names
  transloc2$genes <- gsub("mmest", "MMSET", transloc2$genes) #changes mmest to MMSET
  transloc2$variant_name <- c("t(4;14)", "t(11;14)", "t(14;16)")
  colnames(transloc2) <- c( dnaSampleId, "genes", "variant_name")
  haha <- (merge(transloc2, prognosticMarkerList, by.transloc2 = variant_name))
  translocation_results <- haha %>% filter( haha[,2] > 0)

  # bandaid: NSD2 in fusion file as a backup check / correction for the translocation classifier
  if("t(4;14)" %in% translocation_results$variant_name){
     # filter for the gene partner NSD2 as a translocation in the fusions table
    fusion_results <- read_tsv(fusions_table) %>%
      data.frame() %>%
      filter("NSD2" == `X.gene1` | "NSD2" == `gene2`) %>%
      filter(type == 'translocation')

    # if it is NOT detected (i.e. an empty table after filtering) then remove the transloc call from the results.
    if(nrow(fusion_results) == 0){
      translocation_results <- translocation_results %>% filter(variant_name != 't(4;14)')
    }
  }
} else{
  translocation_results <- data.frame()
}

# CNA prognostic markers, load per_band_cnv file
facet_and_cyto<- read.csv(facetncyt_table, row.names = NULL)
facet_and_cyto$cnv <- as.numeric(as.character(facet_and_cyto$cnv))
variant <- ifelse(facet_and_cyto$cnv == 2, yes = "diploid", no = ifelse(facet_and_cyto$cnv <= 1.5 , yes = "deletion", no = "gain"))

arm <- str_extract(facet_and_cyto$band, "(p|q)")
newchr <- str_split(facet_and_cyto$band, "p|q", simplify = TRUE) [, 1]
chr <- paste("chr", newchr, sep = "")
facetncyt <- cbind(facet_and_cyto, chr, arm, variant)
facetncyt <- subset(facetncyt, select = -c(band, cnv))
lol <- merge(facetncyt, prognosticMarkerList, by = c("variant", "chr", "arm"))
lol1 <- unique(lol)
cna_prog_markers_results <- lol1

# Special case 1: Bi-allelic "double-hit" - 17p deletion + TP53 mutation
somTable <- read.table(annotated_variants_path, sep='\t', fill=TRUE, comment.char='#', quote='', row.names=NULL, stringsAsFactors=FALSE, header=TRUE)
mydata <- somTable[somTable$Hugo_Symbol == "TP53", ]
mydata1 <- unique(mydata)
if("chr17p deletion" %in% lol1$variant_name && nrow(mydata1)>0 && length(mydata1)>0){
  doublehit <- ifelse("TP53" %in% mydata1$Hugo_Symbol, "TP53 mutation + loss (bi-allelic inactivation)", "NO")
  doublehit_add <- data.frame(variant_name = doublehit)
  doublehit_results <- merge(doublehit_add,prognosticMarkerList, by= c("variant_name"))
} else{
  doublehit_results <- data.frame()
}

# Special case 2: t(4:14; 1q gain)
test <- ifelse("chr1q gain" %in% cna_prog_markers_results$variant_name
 && "t(4;14)" %in% translocation_results$variant_name,
yes = "t(4;14) + 1q gain", no = "NO")
test <- as.data.frame(test)
colnames(test) <- c("variant_name")
transloc_and_cna_results <- merge(test, prognosticMarkerList, by.test = "variant_name")

# Special case 3: Hyperdiploidy
hyperdiploidy <- subset(facetncyt, variant == "gain" & chr %in% c("chr21", "chr3", "chr5", "chr7", "chr9", "chr11","chr15", "chr19"))
hyperdiploidy <- hyperdiploidy %>% distinct(chr)
hyperdiploidy <- ifelse(nrow(hyperdiploidy) >= 2, "hyperdiploidy", "NO")
hyperdiploidy <- as.data.frame(hyperdiploidy)
colnames(hyperdiploidy) <- c("variant_name")
hyperdiploidy_results <- merge(hyperdiploidy, prognosticMarkerList, by.hyperdiploidy = "variant_name") 

# Special Case 4: CNV + SCAR score (low SCAR + chr9 gain = good prognosis)
scarMat <- read.delim(scar_score_path)
scarMat$isPt <- ifelse(grepl(dnaSampleId, scarMat$sampleID), "yes", "no")
scarMat$riskCategory <- cut(scarMat$HRD.sum, breaks = quantile(scarMat$HRD.sum), labels = c("low", "moderate", "moderate-high", "high"), include.lowest=TRUE, ordered_result=TRUE)
scarScore <- round(scarMat[scarMat$isPt=="yes","HRD.sum"], 2)
scarCatPatient <- as.character(scarMat[scarMat$isPt=="yes","riskCategory"])
scarMat$variant_name <- ifelse(scarMat$isPt == "yes" && scarCatPatient == "low" && "chr9" %in% facetncyt$chr && "gain" %in% facetncyt$variant, yes = "Low GSS + chr9 gain", no = "no")
scar_score_results <- merge(scarMat, prognosticMarkerList, by.scarMat = "variant_name")

# Results - Output to a CSV File
all_results <- bind_rows(translocation_results, cna_prog_markers_results, doublehit_results, hyperdiploidy_results, scar_score_results)
final_results <- unique(all_results[, c("variant_name", "variant_type", "variant", "gene", "chr", "arm", "prognostic_value", "prevalence_percent", "prevalence_statement", "locus_description", "prognostic_statment", "pmid")])
write.csv(final_results, paste(output_path, "mm_hallmarks_results.csv", sep = '/'))
