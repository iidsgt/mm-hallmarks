cwltool trialcheck.cwl \
--ProgMarkTable "/data1/users/meghana/myelomahallmarks/prognosticMarkersTable.tsv" \
--dnaSampleID "MM_4357_02_01" \
--TranslocTable "/data1/users/meghana/data/MM_4357_02_01/predicted_translocations.csv" \
--FacetncytTable "/data1/users/meghana/myelomahallmarks/cnv_band_table_MM_4357_02_01.csv" \
--ScarScorePath "/data1/users/meghana/myelomahallmarks/scarScore_latest.tsv" \
--AnnotatedVariantsPath "/data1/users/meghana/data/MM_4357_02_01/annotated_variants.consensus.vcf" \
--outdir "/data1/users/meghana/myelomahallmarks/"
