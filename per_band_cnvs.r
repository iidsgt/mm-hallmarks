# from the risk score input generator r script
# use this to make input data from the facets_cncf files.library(GenomicRanges)
#### read in tables and set up columns
parse_cnv <- "/data1/users/meghana/data/MM_6000_01_01/tumor.facets_cncf.txt"
cytoband <- "/data1/users/meghana/MM-PSN/test_input/RiskScoreInputGenerator/cytoBand_hg38.txt"
parsed_cnv_tab <- read.table(parse_cnv, sep = "\t", header = T)
cytoband_tab <- read.table(cytoband, sep = "\t", header = F)
cytoband_tab$tag <- paste0(gsub("chr","",cytoband_tab$V1),cytoband_tab$V4) # nolint
cytoband_tab$rawchr <- gsub("chr","",cytoband_tab$V1)
#### generate real CNV by taking into account cellularity####
parsed_cnv_tab$real_cnv <- (parsed_cnv_tab$cf.em*parsed_cnv_tab$tcn.em) + ((1-parsed_cnv_tab$cf.em)*2)
##generate Granges Objects ######
library(GenomicRanges)
cytoband_GR <- GRanges(seqnames = cytoband_tab$V1,
ranges = IRanges(start = cytoband_tab$V2,
end = cytoband_tab$V3,
band = cytoband_tab$tag))
# to install MultiDataSet:
# if (!requireNamespace("BiocManager", quietly = TRUE))
#     install.packages("BiocManager")
#     BiocManager::install("MultiDataSet")
library(MultiDataSet)
parsed_cnv_tab$chrom <- chrNumToChar(parsed_cnv_tab$chrom)
parse_cnv_GR <- GRanges(seqnames = parsed_cnv_tab$chrom,
ranges = IRanges(start = parsed_cnv_tab$start,
end = parsed_cnv_tab$end,
cnv = parsed_cnv_tab$real_cnv))
###find overlapping regions between objects #####
Overlaps <- findOverlaps(parse_cnv_GR,cytoband_GR)
#### make band x cnv table #####
CNV <- parsed_cnv_tab$real_cnv[Overlaps@from]
bands <- cytoband_tab$tag[Overlaps@to]
names(CNV) <- bands
cnv_band_table <- t(as.matrix(CNV))
#changing matrix to dataframe to export as CSV 
df_mtx <- data.frame( band = bands, copy_number = CNV)
# exporting dataframe as CSV file 
write.table(df_mtx, "/data1/users/meghana/myelomahallmarks/cnv_band_table.csv", sep = ",", row.names = FALSE)