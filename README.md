README file for trialcheck.r, a R script for identifying myeloma hallmarks in a patient from a reference table. The R script takes 7 arguments, 5 of which are paths to input files, 1 is a string containing sample ID and the last argument is the path to an output file.

The hallmarks looks for translocations, cnas, hyperdiploidy, a combination of translocation and cnvs, TP53 mutation and cnvs and combination of scar score and cnv, which indicate severity of the disease and prognosis of the patient.

Test files:
* prog_mark_table <- "/data1/users/meghana/myelomahallmarks/prognosticMarkersTable.tsv" 
* transloc_table <- "/data1/users/meghana/data/MM_6000_01_01/predicted_translocations.csv"
* facetncyt_table <- "/data1/users/meghana/myelomahallmarks/cnv_band_table.csv" 
* scar_score_path <- "/data1/users/meghana/myelomahallmarks/scarScore_latest.tsv" 
* annotated_variants_path <- "/data1/users/meghana/myelomahallmarks/annotated_variants.consensus.vcf" 
* output_path <- "/data1/users/meghana/myelomahallmarks/final_results.csv" 
* dnaSampleId <- "MM_6000_01_01"

Input files:
* Prog_mark_table <- "Path to a reference table"
* transloc_table <- " predicted_translocations.csv under each patient"
* facetncyt_table <- " cnv_band_table.csv should be under each patient, but before integration can be obtained by running per_band_cnvs.r Rscript"
* scar_score_path <- " SCAR score for patients"
* annotated_variants_path <- "annotated_variants.consensus.vcf file under each patient"
* dnaSampleId <- "sample id for the patient typically in the pattern MM_xxxx_xx_xx"

Output file(s):
* output_path <- " Path to output file"