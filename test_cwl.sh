#SAMPLEID="MM_0630_BM_02"
#SAMPLEID="MM_6080_BM_01"
SAMPLEID="MM_0621_BM_04"
cwltool trialcheck.cwl \
--ProgMarkTable "/data1/users/meghana/myelomahallmarks/prognosticMarkersTable.tsv" \
--dnaSampleID "${SAMPLEID}" \
--TranslocTable "/data1/users/meghana/data/${SAMPLEID}/secondary/predicted_translocations.csv" \
--FacetncytTable "/data1/users/meghana/data/${SAMPLEID}/secondary/cnv_per_band.csv" \
--ScarScorePath "/data1/users/meghana/myelomahallmarks/scarScore_latest.tsv" \
--AnnotatedVariantsPath "/data1/users/meghana/data/${SAMPLEID}/primary/annotated_variants.consensus.vcf" \
--outdir "/data1/users/meghana/data/${SAMPLEID}"
 