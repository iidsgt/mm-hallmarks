#SAMPLEID="MM_0630_BM_02"
#SAMPLEID="MM_6080_BM_01"
SAMPLEID="MM_0621_BM_04"
docker build -t sinaiiidgst/myelomahallmarks:test .

docker run \
-v $PWD/data/:/refdata/ \
sinaiiidgst/myelomahallmarks:test \
Rscript /bin/trialcheck.r \
/refdata/prognosticMarkersTable.tsv \
${SAMPLEID} \
/refdata/predicted_translocations.csv \
/refdata/cnv_per_band.csv \
/refdata/scarScore_latest.tsv \
/refdata/annotated_variants.consensus.vcf \
/refdata/ \
/refdata/fusions_positive.tsv
